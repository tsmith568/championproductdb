﻿CREATE TABLE [dbo].[Product] (
    [ProductId]   INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [CreatedDate] DATETIME     CONSTRAINT [DF_Product_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]   BIT          CONSTRAINT [DF_Product_IsEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

