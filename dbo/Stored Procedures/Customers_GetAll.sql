﻿-- =============================================
-- Author:		Tabb Smith
-- Create date: 04/12/2021
-- Description:	Get All Customers from the Customer DB.
-- =============================================
CREATE PROCEDURE [dbo].[Customers_GetAll]

AS
BEGIN
	SET NOCOUNT ON;


	SELECT [CustomerId]
		  ,[CustomerTypeId]
		  ,[BusinessName]
		  ,[LastName]
		  ,[FirstName]
		  ,[AddressId]
		  ,[CreatedDate]
		  ,[IsEnabled]
	  FROM [dbo].[Customer]
	 ORDER BY [FirstName]

END